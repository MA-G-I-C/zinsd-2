import csv
if __name__ == "__main__":
    with open("dns.log") as dns_log:
        with open("hosts") as hosts:
            h_a = 0
            h_u = 0
            h_arr = []
            dns = csv.reader(dns_log, delimiter="\x09")
            for line in hosts:
                h_arr.append(line.strip())
            for line in dns:
                try:
                    h_a += 1
                    if line[9] in h_arr:
                        h_u += 1
                        print("Unwanted Host: " + line[9])
                except IndexError:
                    pass
            print("Unwanted Traffic Percentage: " + str(h_u * 100 / h_a) + "%")
